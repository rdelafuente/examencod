/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tienda;

/**
 *
 * @author Ruben
 */
public class Producto {
    private String nombre;
    private double precio;
    
    
    public Producto(){
           
    }
    
    /*
     * Constructor donde declaramos los atributos nombre y precio
     */
    public Producto (String nombre, double precio){
        
        this.nombre=nombre;
        this.precio=precio;
    }

    /**
     * @return the nombre
     * devuelve el nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * 
     * @param nombre the nombre to set
     * para añadir el nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the precio
     * devuelve el precio
     */
    public double getPrecio() {
        return precio;
    }

    /**
     * @param precio the precio to set
     * para añadir el precio
     */
    public void setPrecio(double precio) {
        this.precio = precio;
    }
    
    
    
    
}
